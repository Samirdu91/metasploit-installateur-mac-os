#!/bin/sh
echo "#************************************************#
      #                   Metasploit.sh                #
      #                   ecrit par M.samir            #
      #                   18/08/2015                   #
      #                                                #
      #                   Installeur metasploit        #
      #************************************************# "
sleep 2
echo "
Metasploit Installeur for Yosemite 
Certaines commandes vous demande votre mot de passe cela est normal 
L'installation va se lancer il faut installer jdk java Xcode avant de lancer le script  "
sleep 3
clear 
echo " faire Install puis agree et attendre la fin de l'installation "
xcode-select --install
echo "
Script en cours :) 
En attendant fait un mini café "
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor
echo PATH=/usr/local/bin:/usr/local/sbin:$PATH >> ~/.bash_profile
source ~/.bash_profile
brew tap homebrew/versions
brew tap homebrew/dupes
clear
echo "Nmap va s'installer "
brew install nmap
clear
echo "Au tour de ruby "
brew install homebrew/versions/ruby21
clear
echo " Au tour de la DB "
brew install postgresql --without-ossp-uuid
clear
echo "Deplacement de fichier pour lancer la db au boot du pc "
mkdir -p ~/Library/LaunchAgents
cp /usr/local/Cellar/postgresql/9.4.1/homebrew.mxcl.postgresql.plist ~/Library/LaunchAgents/
echo " Db va s'allumer "
launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
clear
echo " création de l'user "
createuser msf -P -h localhost
createdb -O msf msf -h localhost
echo " Quel mot de passe avez vous mis juste en haut "
read motdepass
sleep 2
clear
echo "Configuration de vnc en cours "
echo '#!/usr/bin/env bash'>> /usr/local/bin/vncviewer
echo open vnc://\$1 >> /usr/local/bin/vncviewer
chmod +x /usr/local/bin/vncviewer
clear 
echo "installation de metasploit"
gem install pg sqlite3 msgpack activerecord redcarpet rspec simplecov yard bundler
cd /usr/local/share/
git clone https://github.com/rapid7/metasploit-framework.git
cd metasploit-framework
for MSF in $(ls msf*); do ln -s /usr/local/share/metasploit-framework/$MSF /usr/local/bin/$MSF;done
sudo chmod go+w /etc/profile
exit
sudo echo export MSF_DATABASE_CONFIG=/usr/local/share/metasploit-framework/config/database.yml >> /etc/profile
exit
bundle install
clear
echo "Metasploit installe mais c'est pas fini "
clear
echo dans le fichier mettez cela 
echo "production:
      adapter: postgresql
      database: msf
      username: msf
      password: $motdepass
      host: 127.0.0.1
      port: 5432
      pool: 75
     timeout: 5 "
sudo vi /usr/local/share/metasploit-framework/config/database.yml
echo "Presque Fini "
exit
source /etc/profile
source ~/.bash_profile
echo "Metasploit Installe "
sleep 2
clear
echo " Au tour de Armitage maitenant "
brew install pidof
curl -# -o /tmp/armitage.tgz http://www.fastandeasyhacking.com/download/armitage-latest.tgz
tar -xvzf /tmp/armitage.tgz -C /usr/local/share
bash -c "echo \'/usr/bin/java\' -jar /usr/local/share/armitage/armitage.jar \$\*" > /usr/local/share/armitage/armitage
perl -pi -e 's/armitage.jar/\/usr\/local\/share\/armitage\/armitage.jar/g' /usr/local/share/armitage/teamserver
clear
ln -s /usr/local/share/armitage/armitage /usr/local/bin/armitage
ln -s /usr/local/armitage/teamserver /usr/local/bin/teamserver
clear 
echo "Pour lancer Armitage : sudo –E armitage
Pour lancer Metasploit : sudo -E msfconsole"